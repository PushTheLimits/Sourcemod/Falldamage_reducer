#pragma semicolon 1

// Plugin Info
#define FDR_PLUGIN_VERSION "${-version-}" // Version is replaced by the GitLab-Runner compile script
#define FDR_PLUGIN_NAME "CS:S  Falldamage reducer"
#define FDR_PLUGIN_AUTHOR "Lightningblade"
#define FDR_PLUGIN_DESCRIPTION "Reduces the falldamage by a configureable config value"
#define FDR_PLUGIN_WEBSITE "https://gitlab.com/PushTheLimits/Sourcemod/Falldamage_reducer"


#include <sourcemod>
#include <sdktools>
#include <sdkhooks>

#pragma newdecls required

public Plugin myinfo = 
{
	name = FDR_PLUGIN_NAME,
	author = FDR_PLUGIN_AUTHOR,
	description = FDR_PLUGIN_DESCRIPTION,
	version = FDR_PLUGIN_VERSION,
	url = FDR_PLUGIN_WEBSITE

};

static ConVar g_FalldamageReductionPercentage;
static ConVar g_FalldamageReductionFlat;

public void OnPluginStart()
{
	g_FalldamageReductionPercentage = CreateConVar("sm_reduce_fall_damage_percentage", "0.0", "Reduces the falldamage by a factor");
	g_FalldamageReductionFlat = CreateConVar("sm_reduce_fall_damage_flat", "0.0", "Adds a threshhold for minimun damage");
		
	// Account for late loading
	for(int i=1;i<=MaxClients;i++)
	{
		if(IsClientInGame(i))
		{
			OnClientPutInServer(i);
		}	
	}
}

public void OnClientPutInServer(int client)
{
	SDKHook(client, SDKHook_OnTakeDamage, Hook_OnTakeDamage);
}


public Action Hook_OnTakeDamage(int client, int &attacker, int &inflictor, float &damage, int &damagetype)
{	
	float falldamageMultiplier = GetConVarFloat(g_FalldamageReductionPercentage);
	float falldamageThreshhold = GetConVarFloat(g_FalldamageReductionFlat);
	if (damagetype & DMG_FALL)
	{
		damage -= ((damage * falldamageMultiplier) + falldamageThreshhold);
		if (falldamageMultiplier >= 1.0 || damage <= 0)
		{
			return Plugin_Handled; //No damage
		}
		return Plugin_Changed; //Damage value changed
	}
	
	return Plugin_Continue;
}
